#include <bits/stdc++.h>
#include<string>
#include <time.h>
#include <sstream>
using namespace std;


int main() {
	clock_t tic = clock();
	  
	int n;
	cin >> n;
	
	std::ostringstream s;
	s << n;
	
	int sum = 0;
	int power = s.str().length();
	

	for(int i = 0;i < power;i++) {
		sum += pow(s.str()[i] - '0', power);
	}
    
    if(sum == n)
	    cout<<"Armstrong\n";
	else 
		cout<<"NOT\n";
		
clock_t toc = clock();

    printf("Elapsed: %f seconds\n", (double)(toc - tic) / CLOCKS_PER_SEC);
	return 0;
}