#include <bits/stdc++.h>
#include<string>
#include <omp.h>
#include <sstream>
using namespace std;


int main() {
	double time = omp_get_wtime();
	  
	int n;
	cin >> n;
	
	std::ostringstream s;
	s << n;
	
	int sum = 0;
	int power = s.str().length();
	
	omp_set_num_threads(power);
	
	#pragma omp parallel for reduction(+: sum)
	for(int i = 0;i < power;i++) {
		sum += pow(s.str()[i] - '0', power);
	}
    
    if(sum == n)
	    cout<<"Armstrong\n";
	else 
		cout<<"NOT\n";
		
	printf("Executed in %f Seconds \n",omp_get_wtime() - time); 
	return 0;
}
