#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>

int main(int argc, char *argv[]) { 


    int rank, no_of_threads , size = 10000;
    
    MPI_Init(&argc, &argv);  
    MPI_Comm_size(MPI_COMM_WORLD, &no_of_threads);  
    MPI_Comm_rank(MPI_COMM_WORLD, &rank); 
    
    int send_arr[size*no_of_threads],receive_arr[size*no_of_threads];  
        
    for (int i = 0; i < size; i++)
        send_arr[i] = rand() % 50;

   
    double total = 0.0,start = 0.0;  

    for (int i = 0; i < 100; i++) {

        MPI_Barrier(MPI_COMM_WORLD);

        start = MPI_Wtime();

        MPI_Alltoall(send_arr,size,MPI_CHAR,receive_arr,size,MPI_CHAR,MPI_COMM_WORLD);  

        MPI_Barrier(MPI_COMM_WORLD);    

        total += (MPI_Wtime() - start);  
    }  

    if (rank == 0)  
        printf("time for alltoall broadcast=%f Seconds\n", total/100);  
        
    MPI_Finalize();
}
